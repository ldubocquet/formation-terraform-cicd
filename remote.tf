terraform {
  backend "gcs" {
    // Update value with the gcs bucket used to save state files
    bucket = "ldubocquet-step7-tfstate"
  }
}
