variable "application_name" {}
variable "machine_type" {}
variable "gcp_project" {}
variable "zone" {
  default = "europe-west1-b"
}
