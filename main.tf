resource "google_compute_instance" "default" {
  // Create a new gce instance with a dummy startup script and a debian-9 image
  name = var.application_name
  project = var.gcp_project
  machine_type = var.machine_type
  zone = var.zone

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
}
